import React, { useEffect, useState } from 'react';
import { countWpm } from '../utils/countWpm';

const useCountWpm = (incorrectCount, totalLetter, isEnd) => {
  const [wpm, setWpm] = useState(0);

  useEffect(() => {
    if (!isEnd) {
      setWpm(countWpm(incorrectCount, totalLetter));
    }
  }, [incorrectCount, totalLetter, isEnd]);

  return [wpm];
};

export default useCountWpm;
