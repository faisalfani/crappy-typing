import Head from 'next/head';
import React from 'react';
import { DynamicComponentWithNoSSR } from '../components';
import Container from './styled';
export default function Home() {
  return (
    <>
      <Head>
        <link rel='shortcut icon' href='/favicon.ico' />
        <title>Crappytyping</title>
      </Head>
      <Container>
        <DynamicComponentWithNoSSR />
      </Container>
    </>
  );
}
