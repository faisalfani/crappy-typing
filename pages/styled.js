import styled from '@emotion/styled';

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  height: 100vh;
  font-family: 'Roboto Mono', monospace;
  background-image: #ca99ff;
  background-image: radial-gradient(
      at 16% 24%,
      hsla(204, 84%, 77%, 1) 0,
      transparent 44%
    ),
    radial-gradient(at 81% 61%, hsla(285, 78%, 61%, 1) 0, transparent 41%),
    radial-gradient(at 69% 86%, hsla(15, 61%, 60%, 1) 0, transparent 53%),
    radial-gradient(at 7% 8%, hsla(11, 60%, 72%, 1) 0, transparent 42%),
    radial-gradient(at 32% 44%, hsla(322, 71%, 64%, 1) 0, transparent 41%),
    radial-gradient(at 41% 66%, hsla(48, 75%, 65%, 1) 0, transparent 53%),
    radial-gradient(at 16% 12%, hsla(206, 88%, 77%, 1) 0, transparent 47%);
`;

export default Container;
