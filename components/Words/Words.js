import { CrappyCountdown, Word } from '..';
import { WordsContainer } from './styled';
import useKeyPress from '../../hooks/useKeyPress';
import { useEffect, useState } from 'react';
import { ScaleFade, useDisclosure } from '@chakra-ui/react';
import SuccessModal from '../SuccessModal';

const Words = () => {
  const [start, setStart] = useState(false);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [isEnd, setIsEnd] = useState(false);
  const { restructuredData, currentWord, isStart, setIsStart, wpm } =
    useKeyPress(isEnd);

  useEffect(() => {
    setStart(isStart);
  }, [isStart]);

  useEffect(() => {
    setIsEnd(isOpen);
  }, [isOpen]);

  return !isEnd ? (
    <ScaleFade in={!isEnd}>
      <WordsContainer>
        {isStart && <CrappyCountdown isStart={isStart} onOpen={onOpen} />}
        {restructuredData &&
          restructuredData.map((word) => {
            return (
              <Word word={word} key={word._id} currentWord={currentWord} />
            );
          })}
      </WordsContainer>
    </ScaleFade>
  ) : (
    <SuccessModal
      isOpen={isOpen}
      onClose={onClose}
      setIsStart={setIsStart}
      wpm={wpm}
    />
  );
};

export default Words;
