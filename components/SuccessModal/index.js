import React from 'react';
import {
  Box,
  Heading,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  Text,
} from '@chakra-ui/react';
import { RepeatClockIcon } from '@chakra-ui/icons';

const SuccessModal = ({ isOpen, onClose, setIsStart, wpm }) => {
  const onCloseClicked = () => {
    setIsStart(false);
    onClose();
  };
  return (
    <Modal
      isOpen={isOpen}
      onClose={onCloseClicked}
      isCentered
      size={'xl'}
      closeOnEsc
    >
      <ModalContent bg={'whiteAlpha.300'} textAlign='center'>
        <ModalBody
          display='flex'
          flexDirection='column'
          alignItems='center'
          rowGap={'12px'}
          paddingBottom={28}
          paddingTop={10}
        >
          <Box display='flex' flexDirection='column' rowGap='2px'>
            <Heading as={'h2'} color='gray.50'>
              Congratulation 🥳
            </Heading>
            <Text color='gray.200'>Your wpm on 30 seconds is</Text>
          </Box>
          <Text
            fontSize='8xl'
            bgGradient='linear(to-l, #7928CA, #FF0080)'
            bgClip='text'
            fontWeight='extrabold'
          >
            {wpm} wpm
          </Text>
          <RepeatClockIcon
            color='whiteAlpha.700'
            w={8}
            h={8}
            onClick={onCloseClicked}
            cursor='pointer'
            _hover={{
              color: 'whiteAlpha.600',
            }}
          />
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default SuccessModal;
