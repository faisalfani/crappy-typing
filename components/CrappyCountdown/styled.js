import styled from '@emotion/styled';

export const CountdownText = styled.span`
  font-size: x-large;
  color: black;
  width: 79vw;
  padding: 1rem 2rem;
  position: absolute;
  top: -4rem;
  left: 0;
`;
