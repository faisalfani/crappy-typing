import { useDisclosure } from '@chakra-ui/react';
import React from 'react';
import Countdown from 'react-countdown';
import { CountdownText } from './styled';

const CrappyCountdown = ({ onOpen }) => {
  const renderer = ({ seconds, completed }) => {
    if (completed) {
      return <></>;
    } else {
      return <CountdownText>{seconds}</CountdownText>;
    }
  };

  return (
    <>
      <Countdown
        renderer={renderer}
        date={Date.now() + 60000 * 0.25}
        onComplete={onOpen}
      />
    </>
  );
};

export default React.memo(CrappyCountdown);
