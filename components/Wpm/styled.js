import { Box } from '@chakra-ui/react';
import styled from '@emotion/styled';

export const WpmWrapper = styled(Box)`
  position: absolute;
  bottom: 1rem;
  left: 2rem;
`;
