export const countWpm = (incorrectCount, totalLetter) => {
  const result = Math.floor((totalLetter / 5 - incorrectCount) / 0.25);
  return result > 0 ? result : 0;
};
